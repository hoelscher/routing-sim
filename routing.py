import socket
import threading
import json
import time

class Router:
    def __init__(self, id, ip, port, neighbors):
        self.id = id
        self.ip = ip
        self.port = port
        self.neighbors = neighbors  # List of (neighbor_id, ip, port, distance)

        self.routing_table = []
        for n in self.neighbors:
            self.routing_table.append({"id": n["id"],
                                        "next_hop": n["id"],
                                        "distance": n["distance"]})
        self.lock = threading.Lock()

    def start(self):
        threading.Thread(target=self.listen_for_neighbors).start()
        threading.Thread(target=self.send_routing_table).start()

    def listen_for_neighbors(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_socket.bind((self.ip, self.port))
        print(f"Router {self.id} listening on {self.ip}:{self.port}")

        while True:
            data, _ = server_socket.recvfrom(1024)
            routing_info = json.loads(data.decode())

            try:
                json.loads(data.decode())
            except json.JSONDecodeError:
                continue

            self.update_routing_table(routing_info)

    def send_routing_table(self):
        while True:
            time.sleep(2)  # Send routing table every 5 seconds
            with self.lock:
                routing_info = {"sender_id": self.id,
                                "routes":[{"id": r["id"],
                                            "distance": r["distance"]} for r in self.routing_table]}
                routing_message = json.dumps(routing_info).encode()
            for n in self.neighbors:
                client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                client_socket.sendto(routing_message, (n["ip"], n["port"]))
                print(f"Sent to {n['ip']}")

            print("Periodic info: ")
            print(self.routing_table)

    def update_routing_table(self, routing_info):
        next_hop = routing_info["sender_id"]

        distance_to_neighbour = float('inf')
        for n in self.neighbors:
            if n["id"] == next_hop:
                distance_to_neighbour = n["distance"]
                break

        for route in routing_info["routes"]:
            id = route["id"]
            if id == self.id:
                continue
            distance = route["distance"]

            # check if the distance is shorter with a hop over this neighbour
            existing_route = next((r for r in self.routing_table
                                   if r["id"] == id), None)
            if existing_route:
                existing_distance = existing_route["distance"]
                if distance + distance_to_neighbour < existing_distance:
                    route["next_hop"] = next_hop
                    route["distance"] = distance + distance_to_neighbour
                    self.routing_table.remove(existing_route)
                    self.routing_table.append(route)
                    print(f"Router {self.id} updated routing table: {self.routing_table}")
            else:
                route["next_hop"] = next_hop
                route["distance"] = distance + distance_to_neighbour
                self.routing_table.append(route)
                print(f"Router {self.id} updated routing table: {self.routing_table}")

if __name__ == "__main__":
    # For testing:
    # Reads the configuration of the network from the file 'routernetz_local.json'
    # and creates a list of routers. Then it starts each router.
    # In production use, only one router is started and is being provided with a list of neighbors.

    # Beispielkonfiguration
    #with open('routernetz_local.json', encoding='utf-8') as f:
    #    routernetz = json.load(f)

    #routers=[]

    #for r in routernetz["routers"]:
    #    routers.append(Router(r["id"], r["ip"], r["port"], r["connections"]))

    #for router in routers:
    #    router.start()
    router = Router("Hls", "10.16.4.100", 3333, [
        {"id": "Jimi", "ip": "10.16.33.162", "port": 3333, "distance": 12},
        {"id": "Quin", "ip": "10.16.4.27", "port": 3333, "distance": 9},
        {"id": "Luan", "ip": "10.16.4.26", "port": 3333, "distance": 6},
        {"id": "Tim", "ip": "10.16.4.35", "port": 3333, "distance": 13}
      ])
    router.start()